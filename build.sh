#!/bin/bash
antlr="antlr-4.13.1-complete.jar"
#antlr="antlr-4.11.1-complete.jar"
java -jar $antlr -o ./java -visitor LML.g4
cd java
javac -cp ../$antlr *.java
cd ..
java -jar  $antlr -o ./golang/parser -visitor -Dlanguage=Go LML.g4

