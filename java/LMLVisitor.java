// Generated from LML.g4 by ANTLR 4.13.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LMLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LMLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link LMLParser#template}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemplate(LMLParser.TemplateContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#templateMeta}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemplateMeta(LMLParser.TemplateMetaContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#property}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProperty(LMLParser.PropertyContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#pdfProperty}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPdfProperty(LMLParser.PdfPropertyContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#pdfSettingsBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPdfSettingsBlock(LMLParser.PdfSettingsBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#pdfPrefaceBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPdfPrefaceBlock(LMLParser.PdfPrefaceBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#propertyBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPropertyBlock(LMLParser.PropertyBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#section}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSection(LMLParser.SectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#subsection}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubsection(LMLParser.SubsectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#subsubsection}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubsubsection(LMLParser.SubsubsectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(LMLParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(LMLParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#ifClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfClause(LMLParser.IfClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#elseIfClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseIfClause(LMLParser.ElseIfClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#elseClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseClause(LMLParser.ElseClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#htmlBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlBlock(LMLParser.HtmlBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#ldp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLdp(LMLParser.LdpContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#pageBreak}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPageBreak(LMLParser.PageBreakContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#pdfBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPdfBlock(LMLParser.PdfBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#repeatBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeatBlock(LMLParser.RepeatBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#repeatTimes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeatTimes(LMLParser.RepeatTimesContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#restoreDate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestoreDate(LMLParser.RestoreDateContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#restoreMcDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestoreMcDay(LMLParser.RestoreMcDayContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#restoreRealm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestoreRealm(LMLParser.RestoreRealmContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#restoreVersion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestoreVersion(LMLParser.RestoreVersionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#setMcDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetMcDay(LMLParser.SetMcDayContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#setRealm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetRealm(LMLParser.SetRealmContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#setVersion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetVersion(LMLParser.SetVersionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#switchStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchStatement(LMLParser.SwitchStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#switchLabel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchLabel(LMLParser.SwitchLabelContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#dowName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDowName(LMLParser.DowNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#insert}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsert(LMLParser.InsertContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#anchor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnchor(LMLParser.AnchorContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#href}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHref(LMLParser.HrefContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#hrefId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHrefId(LMLParser.HrefIdContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#target}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTarget(LMLParser.TargetContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#targetValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTargetValue(LMLParser.TargetValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#anchorLabel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnchorLabel(LMLParser.AnchorLabelContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#anchorLabelId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnchorLabelId(LMLParser.AnchorLabelIdContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#image}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImage(LMLParser.ImageContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#source}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSource(LMLParser.SourceContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#title}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTitle(LMLParser.TitleContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#width}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWidth(LMLParser.WidthContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#height}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHeight(LMLParser.HeightContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#h1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitH1(LMLParser.H1Context ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#h2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitH2(LMLParser.H2Context ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#h3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitH3(LMLParser.H3Context ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#para}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPara(LMLParser.ParaContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#pspan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPspan(LMLParser.PspanContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#span}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpan(LMLParser.SpanContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#media}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMedia(LMLParser.MediaContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#nid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNid(LMLParser.NidContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#lid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLid(LMLParser.LidContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#monthName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMonthName(LMLParser.MonthNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#ldpType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLdpType(LMLParser.LdpTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#position}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPosition(LMLParser.PositionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#positionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPositionType(LMLParser.PositionTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#directive}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDirective(LMLParser.DirectiveContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#lookup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLookup(LMLParser.LookupContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#insertDate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertDate(LMLParser.InsertDateContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#insertH1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertH1(LMLParser.InsertH1Context ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#insertH2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertH2(LMLParser.InsertH2Context ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#insertH3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertH3(LMLParser.InsertH3Context ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#rid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRid(LMLParser.RidContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#override}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverride(LMLParser.OverrideContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#overrideMode}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverrideMode(LMLParser.OverrideModeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#overrideDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverrideDay(LMLParser.OverrideDayContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#blankLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlankLine(LMLParser.BlankLineContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#horizontalRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHorizontalRule(LMLParser.HorizontalRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#sid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSid(LMLParser.SidContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplDate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplDate(LMLParser.TmplDateContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplCalendar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplCalendar(LMLParser.TmplCalendarContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplCss}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplCss(LMLParser.TmplCssContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplDay(LMLParser.TmplDayContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplID}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplID(LMLParser.TmplIDContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplModel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplModel(LMLParser.TmplModelContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplMonth}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplMonth(LMLParser.TmplMonthContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplOutput}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplOutput(LMLParser.TmplOutputContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageHeaderEven}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageHeaderEven(LMLParser.TmplPageHeaderEvenContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageFooterEven}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageFooterEven(LMLParser.TmplPageFooterEvenContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageHeaderFirst}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageHeaderFirst(LMLParser.TmplPageHeaderFirstContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageFooterFirst}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageFooterFirst(LMLParser.TmplPageFooterFirstContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageHeaderOdd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageHeaderOdd(LMLParser.TmplPageHeaderOddContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageFooterOdd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageFooterOdd(LMLParser.TmplPageFooterOddContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageHeader}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageHeader(LMLParser.TmplPageHeaderContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageFooter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageFooter(LMLParser.TmplPageFooterContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplPageNumber}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplPageNumber(LMLParser.TmplPageNumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplStatus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplStatus(LMLParser.TmplStatusContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplIndexLastTitleOverride}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplIndexLastTitleOverride(LMLParser.TmplIndexLastTitleOverrideContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplTitleCodes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplTitleCodes(LMLParser.TmplTitleCodesContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplType(LMLParser.TmplTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplOffice}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplOffice(LMLParser.TmplOfficeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#tmplYear}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplYear(LMLParser.TmplYearContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(LMLParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#caseStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseStatement(LMLParser.CaseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#switchDefault}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchDefault(LMLParser.SwitchDefaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#caseExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseExpression(LMLParser.CaseExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#integerExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerExpression(LMLParser.IntegerExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#integerList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerList(LMLParser.IntegerListContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#dowExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDowExpression(LMLParser.DowExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#ridExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRidExpression(LMLParser.RidExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#dowList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDowList(LMLParser.DowListContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#monthDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMonthDay(LMLParser.MonthDayContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator(LMLParser.OperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link LMLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(LMLParser.ExpressionContext ctx);
}