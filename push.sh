#!/bin/bash
# 1st parameter should have the version number, e.g. v0.0.76
# 2nd parameter should have the commit message, e.g. "moved @Ver to rid and sid from para"
# e.g. ./push.sh v0.0.76 "moved @Ver to rid and sid from para"
# If you omit the version number, a tag will be created using HEAD 
# and you will see warning: refname 'HEAD' is ambiguous
# To fix that, use:
#    git tag -d HEAD
# Do so in the root, lml-go, and lml-js.

cd golang
pwd
git add *
git commit -m "$2"
git tag $1 HEAD
git push origin 
git push origin $1 
cd ..
pwd
git add *
git add lml.g4
git commit -m "$2"
git tag $1 HEAD
git push origin
git push origin $1
