/*
 Antlr grammar for the Liturgical Markup Language (lml) 
 */
grammar LML;
template:
	tmplID templateMeta* property* pdfSettingsBlock? pdfPrefaceBlock? statement* EOF;
templateMeta: tmplStatus | tmplType | tmplOffice;
property: tmplDate
	| tmplCalendar
	| tmplIndexLastTitleOverride
	| tmplModel
	| tmplOutput
	| tmplTitleCodes
	| tmplCss
	| propertyBlock
;
pdfProperty: tmplPageNumber
//	| tmplPageNumberPrefix
	| tmplPageHeaderEven
	| tmplPageHeaderFirst
	| tmplPageHeaderOdd
	| tmplPageFooterEven
	| tmplPageFooterFirst
	| tmplPageFooterOdd
	| tmplPageHeader
	| tmplPageFooter
;
pdfSettingsBlock: 'pdfSettings {' pdfProperty* '}'; 
pdfPrefaceBlock: 'pdfPreface' block;
propertyBlock: '{' property* '}';
section : h1 block;
subsection : h2 block;
subsubsection : h3 block;
statement:
	media
	| image
	| para
	| insert
	| htmlBlock
	| pageBreak
	| pdfBlock
	| ifStatement
	| switchStatement
	| repeatBlock
	| restoreDate
	| restoreMcDay
	| restoreRealm
	| restoreVersion
	| section
	| subsection
	| subsubsection
	| setMcDay
	| setRealm
	| setVersion
	| tmplDate
	| tmplPageNumber
//	| tmplPageNumberPrefix
	| block
	| blankLine
	| horizontalRule;

ifStatement: ifClause elseIfClause* elseClause?;
ifClause: 'if' expression block;
elseIfClause: 'else if' expression block;
elseClause: 'else' block;
htmlBlock: 'htmlOnly' block;
ldp: 'ldp' ldpType*;
pageBreak: 'pageBreak';
pdfBlock: 'pdfOnly' block;
repeatBlock: 'repeat' repeatTimes block;
repeatTimes: INTEGER 'times';
restoreDate: 'restoreDate';
restoreMcDay: 'restoreMovableCycleDay';
restoreRealm: 'restoreRealm';
restoreVersion: 'restoreVersion';
setMcDay: 'movableCycleDay' '=' INTEGER;
setRealm: 'realm' '=' STRING;
setVersion: 'version' '=' INTEGER;
switchStatement:
	'switch' switchLabel '{' caseStatement+ switchDefault? '}';

switchLabel:
	MODE_OF_WEEK
	| EXISTS
	| EOTHINON
	| REALM
	| MOVABLE_CYCLE_DAY
	| LUKAN_CYCLE_DAY
	| SUNDAY_AFTER_ELEVATION_OF_CROSS
	| SUNDAYS_BEFORE_TRIODION
	| DATE
	| DAY_OF_WEEK
	| YEAR;
dowName: SUN | MON | TUE | WED | THU | FRI | SAT;
insert: 'insert' STRING;
anchor: (ANCHOR_TAG | ANCHOR_STYLE) href target? anchorLabel;
ANCHOR_TAG: 'a';
href: 'href' hrefId;
hrefId: lid | nid | sid;
target: 'target' targetValue;
targetValue: TARGET_BLANK | TARGET_PARENT | TARGET_SELF | TARGET_TOP;
anchorLabel: 'label' anchorLabelId;
anchorLabelId: lid | nid | sid;
image: IMAGE_STYLE source title? width? height?;
source: 'src' STRING;
title: 'title' ( ldp | lid | nid | rid | sid | span | pspan)*;
width: 'width' STRING;
height: 'height' STRING;
h1:
	(H1_TAG | H1_STYLE) (lid | nid | rid | sid);
h2:
	(H2_TAG | H2_STYLE) (lid | nid | rid | sid);
h3:
	(H3_TAG | H3_STYLE) (lid | nid | rid | sid);
para:
	(PARA_TAG | PARA_STYLE) (ldp | lid | nid | rid | sid | anchor | span | pspan)+ ;
pspan: LPAREN span+ RPAREN;
span:
	(SPAN_TAG | SPAN_STYLE) (ldp | lid | nid | rid | sid | anchor | pspan)+ USE_AS_NOTE?;
media: 'media' (rid | sid);
nid: 'nid' STRING;
lid: 'lid' STRING;
monthName:
	JAN
	| FEB
	| MAR
	| APR
	| MAY
	| JUN
	| JUL
	| AUG
	| SEP
	| OCT
	| NOV
	| DEC;
ldpType: DOM | DOWN | DOWT| EOW | All 
	| GenDate | GenYear | MCD | MOW | NOP | SAEC | SOL | DOL
	| WOLC | WDOLC |SBT;
All: '@allLiturgicalDayProperties';
DOL: '@lukanCycleElapsedDays';
DOM: '@dayOfMonth'; 
DOWN: '@dayOfWeekAsNumber';  
DOWT: '@dayOfWeekAsText';
EOW: '@eothinon';
GenDate: '@serviceDate'; 
GenYear: '@serviceYear'; 
MCD: '@dayOfMovableCycle'; 
MOW: '@modeOfWeek'; 
NOP: '@nameOfPeriod'; 
SAEC: '@sundayAfterElevationCrossDate';
SBT: '@sundaysBeforeTriodion';	
SOL: '@lukanCycleStartDate';
WDOLC: '@lukanCycleWeekAndDay';
WOLC: '@lukanCycleWeek';

position: positionType directive+;
positionType: LEFT | CENTER | RIGHT;
directive: lookup | insertDate | insertH1 | insertH2 | insertH3 | INSERT_PAGE_NUMBER | lid | nid;
lookup: INSERT_LOOKUP ( sid | rid)+ 'ver' INTEGER;
insertDate: '@date' 'ver' INTEGER;
insertH1: '@h1' 'ver' INTEGER;
insertH2: '@h2' 'ver' INTEGER;
insertH3: '@h3' 'ver' INTEGER;
rid: 'rid' STRING override*;
override: overrideMode | overrideDay | INSERT_VER;
overrideMode: '@mode' INTEGER;
overrideDay: '@day' INTEGER;
blankLine: BREAK_STYLE;
horizontalRule: HR_STYLE;
sid: 'sid' STRING INSERT_VER? repeatTimes?;
tmplDate: (tmplDay tmplMonth tmplYear?) | 
          (tmplDay tmplYear? tmplMonth) | 
		  (tmplYear? tmplMonth tmplDay) |
		  (tmplYear? tmplDay tmplMonth) |
		  (tmplMonth tmplYear? tmplDay) |
		  (tmplMonth tmplDay tmplYear?) 
		  ;
tmplCalendar: 'calendar' '=' STRING;
tmplCss: 'css = ' STRING;
tmplDay: 'day' '=' INTEGER;
tmplID: 'id' '=' STRING;
tmplModel: 'model' '=' STRING;
tmplMonth: 'month' '=' INTEGER;
tmplOutput: 'output' '=' STRING;
tmplPageHeaderEven: 'pageHeaderEven' '=' (NONE | position)+;
tmplPageFooterEven: 'pageFooterEven' '=' (NONE | position)+;
tmplPageHeaderFirst: 'pageHeaderFirst' '=' (NONE | position)+;
tmplPageFooterFirst: 'pageFooterFirst' '=' (NONE | position)+;
tmplPageHeaderOdd: 'pageHeaderOdd' '=' (NONE | position)+;
tmplPageFooterOdd: 'pageFooterOdd' '=' (NONE | position)+;
tmplPageHeader: 'pageHeader' '=' (NONE | position)+;
tmplPageFooter: 'pageFooter' '=' (NONE | position)+;
tmplPageNumber: 'pageNbr' '=' INTEGER;
//tmplPageNumberPrefix: 'pageNbrPrefix' '=' STRING;
//tmplPageNumbersOn: '@pageNumbersOn';
//tmplPageNumbersOff: '@pageNumbersOff';
tmplStatus: 'status' '=' STRING;
tmplIndexLastTitleOverride: 'indexLastTitleOverride' '=' STRING;
tmplTitleCodes: 'indexTitleCodes' '=' STRING;
tmplType: 'type' '=' STRING;
tmplOffice: 'office' '=' STRING;
tmplYear: 'year' '=' INTEGER;


// BLOCKS

block: '{' statement* '}';
caseStatement: caseExpression block;
switchDefault: 'default' ':' block;

caseExpression:
	'case' integerExpression ':'
	| 'case' dowExpression ':'
	| 'case' monthName integerExpression ':'
	| 'case' ridExpression ':'
	| 'case' STRING ':';

// EXPRESSIONS

integerExpression:
	INTEGER
	| INTEGER 'thru' INTEGER
	| integerList;
integerList: INTEGER (',' INTEGER)*;

dowExpression: dowName | dowName 'thru' dowName | dowList;
ridExpression: rid;
dowList: dowName (',' dowName)*;
monthDay: monthName INTEGER;
operator: EQ | NEQ | LT | LTOE | GT | GTOE;
expression:
	'(' expression ')'
	| expression '&&' expression
	| expression '||' expression
	| DATE operator monthDay
	| EXISTS ridExpression
	| REALM EQ STRING
	| DAY_OF_WEEK operator dowName
	| SUNDAY_AFTER_ELEVATION_OF_CROSS operator monthDay
    | EOTHINON operator INTEGER
	| MODE_OF_WEEK operator INTEGER
	| MOVABLE_CYCLE_DAY operator INTEGER
	| LUKAN_CYCLE_DAY operator INTEGER
	| SUNDAYS_BEFORE_TRIODION operator INTEGER
	| YEAR operator INTEGER;

// order of declaration matters
EQ: '==';
NEQ: '!=';
ASSIGNMENT: '=';
LBRACE: '{';
RBRACE: '}';
LPAREN: '(';
RPAREN: ')';
COLON: ':';
SWITCH: 'switch';
AND: '&&';
OR: '||';
GT: '>';
GTOE: '>=';
LT: '<';
LTOE: '<=';
IF: 'if';
ELSEIF: 'else if';
ELSE: 'else';
CASE: 'case';
THRU: 'thru';
NOT: '!';
EXISTS: 'exists';
REALM: 'realm';
TARGET_BLANK: '_blank';
TARGET_PARENT: '_parent';
TARGET_SELF: '_self';
TARGET_TOP: '_top';
MODE_OF_WEEK: 'modeOfWeek';
MOVABLE_CYCLE_DAY: 'movableCycleDay';
JAN: 'jan';
FEB: 'feb';
MAR: 'mar';
APR: 'apr';
MAY: 'may';
JUN: 'jun';
JUL: 'jul';
AUG: 'aug';
SEP: 'sep';
OCT: 'oct';
NOV: 'nov';
DEC: 'dec';
DATE: 'date';
YEAR: 'year';
EOTHINON: 'eothinon';
DAY_OF_WEEK: 'dayOfWeek';
LUKAN_CYCLE_DAY: 'lukanCycleDay';
SUNDAY_AFTER_ELEVATION_OF_CROSS: 'sundayAfterElevationOfCross';
SUNDAYS_BEFORE_TRIODION: 'sundaysBeforeTriodion';
INTEGER: [0-9][0-9]*;
SUN: 'sun';
MON: 'mon';
TUE: 'tue';
WED: 'wed';
THU: 'thu';
FRI: 'fri';
SAT: 'sat';
LEFT: 'left';
CENTER: 'center';
RIGHT: 'right';
NONE: 'none';
ANCHOR_STYLE:
	'a.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
BREAK_STYLE:
	'br.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
HR_STYLE:
	'hr.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
IMAGE_STYLE:
	'img.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
H1_TAG: 'h1';
H2_TAG: 'h2';
H3_TAG: 'h3';
H1_STYLE:
	'h1.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
H2_STYLE:
	'h2.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
H3_STYLE:
	'h3.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
PARA_TAG: 'p';
PARA_STYLE:
	'p.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
SPAN_TAG: 'span';
SPAN_STYLE:
	'span.' ([a-z] | [A-Z] | '_' | '-') (
		[a-z]
		| [A-Z]
		| '_'
		| '-'
		| [0-9]
	)*;
INSERT_LOOKUP: '@lookup';
INSERT_PAGE_NUMBER: '@pageNbr';
INSERT_VER: '@ver';
USE_AS_NOTE: '@note';
STRING:
	'"' ('""' | ~'"')* '"'; // quote-quote is an escaped quote
// hidden from parser, but available in generated code
EOL: [\r\t\u000C\n]+ -> channel(HIDDEN);
// skips
COMMENT
    :   ( '//' ~[\r\n]* '\r'? '\n'
        | '/*' .*? '*/'
        ) -> skip
    ;
WS: [ ]+ -> skip;
UNKNOWN_CHAR: .;

